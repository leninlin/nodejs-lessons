'use strict';

const express = require('express');
const app = express();

const Product = require('../models/Product');

app.get('/(:product_id)?', (req, res, next) => {
  let id = req.params.product_id;
  if (id) {
    return Product.find(id, (err, product) => err ? next(err) : res.send(product.data));
  }

  Product.findAll((err, products) => err ? next(err) : res.send(products.map(product => product.data)));
});

app.post('/', (req, res, next) => {
  if (!Object.keys(req.body).length) {
    return res.status(400).send('Empty data');
  }

  let product = new Product();
  product.setData(req.body);
  product.save((err, product) => err ? next(err) : res.send(product.data));
});

app.put('/:product_id', (req, res, next) => {
  if (!Object.keys(req.body).length) {
    return res.status(400).send('Empty data');
  }

  let id = req.params.product_id;

  Product.find(id, (err, product) => {
    product.setData(req.body);
    product.save((err, product) => err ? next(err) : res.send(product.data));
  });
});

app.delete('/:product_id', (req, res, next) => {
  let id = req.params.product_id;
  if (!products[id]) {
    return res.status(404).send('Product not found');
  }

  Product.find(id, (err, product) => {
    product.delete(err => err ? next(err) : res.send());
  });
});

module.exports = app;

'use strict';

const express = require('express');
const cors = require('cors');
const bodyparser = require('body-parser');

const Mongodb = require('mongodb-custom');

const app = express();

app.set('port', 8080);

app.use(cors());
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: false }));

app.get('/ping', (req, res, next) => {
  res.send('pong');
  next(new Error("qewqerw"))
});

app.use('/user', require('./controllers/User'));
app.use('/catalog', require('./controllers/Catalog'));
app.use('/product', require('./controllers/Product'));

app.use((err, req, res, next) => {
  console.error(err.stack);
  return res.status(500).send(err);
});

let startServer = () => {
	app.listen(app.get('port'), () => {
	  console.log('Express server listening on port', app.get('port'));
	});
};

Mongodb.connect(startServer);

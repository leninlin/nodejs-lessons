'use strict';

var Mongodb = require('mongodb-custom');

class Catalog {
  constructor() {
    this.isNew = true;
    this.data = {};
  }

  static findAll(cb) {
    Mongodb.findAll('catalogs', (err, docs) => {
      if (err) { return cb(err); }

      let catalogs = docs.map(doc => {
        let catalog = new Catalog();
        catalog.isNew = false;
        return catalog.setData(doc);
      });

      cb(err, catalogs);
    });
  }

  static find(id, cb) {
    Mongodb.find('catalogs', id, (err, doc) => {
      if (err) { return cb(err); }

      let catalog = new Catalog();
      catalog.isNew = false;
      catalog.setData(doc);

      cb(err, catalog);
    });
  }

  setData(data) {
    Object.keys(data).map(k => this.data[k] = data[k]);
    return this;
  }

  save(cb) {
    let isSave = (err, id) => {
      if (err) { return cb(err); }
      Catalog.find(id, (err, catalog) => {
        this.data = catalog.data;
        this.isNew = catalog.isNew;
        cb(err, this);
      });
    };

    if (this.isNew) {
      Mongodb.insert('catalogs', this.data, isSave);
    } else {
      Mongodb.update('catalogs', this.data._id, this.data, isSave);
    }
  }

  delete(cb) {
    if (this.isNew) {
      cb(new Error('Can\'t be delete'));
    } else {
      Mongodb.update('catalogs', this.data._id, this.data, cb);
    }
  }
}

module.exports = Catalog;

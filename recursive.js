'use strict';

function delArrayNextTick(arr, cb, n) {
  n = n || 0;
  if (!arr.length) return cb(n);
  arr.shift();
  process.nextTick(() => {
    delArrayNextTick(arr, cb, n + 1);
  });
}

function delArrayImmediate(arr, cb, n) {
  n = n || 0;
  if (!arr.length) return cb(n);
  arr.shift();
  setImmediate(() => {
    delArrayImmediate(arr, cb, n + 1);
  });
}

function delArrayTimeout(arr, cb, n) {
  n = n || 0;
  if (!arr.length) return cb(n);
  arr.shift();
  setTimeout(() => {
    delArrayTimeout(arr, cb, n + 1);
  }, 0);
}

var benchmark = (func, cb) => {
  let array = new Array(100000);
  let start = Date.now();
  let count = func(array, (n) => {
    console.log(func.name, n, Date.now() - start + 'ms');
    cb();
  });
}

benchmark(delArrayNextTick,
  () => benchmark(delArrayImmediate,
    () => benchmark(delArrayTimeout, () => {console.log('end')})));
